/*
 * Small piece of arduino firmware that reads
 * serial input and adjusts the blinking frequency of three LEDs
 * accordingly
 */
 
#include <SimpleTimer.h>

 #define redLed 7
 #define yellowLed 6
 #define greenLed 5
 
//function to toggle the state of a single pin
void toggle(int pin){
  digitalWrite(pin, !digitalRead(pin));
}

//struct that holds all the information about a connected LED
struct LED {
  int pin; //arduino pin the LED is connected to
  int timerID; //timerID of the SimpleTimer library
  void (*toggleFunc)(); //function pointer, that toggles the LED
 };

//define our 3 LEDs
 LED leds[3] = {
  {redLed, -1, [](){toggle(redLed);}},
  {yellowLed, -1, [](){toggle(yellowLed);}},
  {greenLed, -1, [](){toggle(greenLed);}}
 };

//define timer instance
SimpleTimer ledTimer;

String inputString = ""; //String to hold incoming data through serial connection
bool newConfig = false; //flag to indicate if a new config was transmitted via serial

//split string at separator and return substring until next separator
String getValue(String data, char separator, int index){
    int found = 0;
    int strIndex[] = { 0, -1 };
    int maxIndex = data.length() - 1;

    for (int i = 0; i <= maxIndex && found <= index; i++) {
        if (data.charAt(i) == separator || i == maxIndex) {
            found++;
            strIndex[0] = strIndex[1] + 1;
            strIndex[1] = (i == maxIndex) ? i+1 : i;
        }
    }
    return found > index ? data.substring(strIndex[0], strIndex[1]) : "";
}

void setup() {
  //define the 3 pins used for the LEDs as outputs
  for(int i=0; i<3; i++){
    pinMode(leds[i].pin, OUTPUT);
  }

  //setup serial connection with 9600 Baud
  Serial.begin(9600);
}

void loop() {
  //run the timer loop iteration
  ledTimer.run();

  //if new config is available setup new frequencies
  if(newConfig){

    //TODO: handle cases where inputstring is not well formated
    //eg. less than two elements or elements can not be parsed to Ints
    
    //split inputstring into LED number and frequency
    int ledNum = getValue(inputString, ':', 0).toInt();
    int frequ = getValue(inputString, ':', 1).toInt();

    //calculate delay between toggles from frequency
    int delayMs =  1./(float)frequ * 1000.;

    LED &led = leds[ledNum];

    //if LED already has a timer assigned remove it
    if(led.timerID != -1)
      ledTimer.deleteTimer(led.timerID);
      
    //if frequency is 0 turn LED off completely
    if(frequ == 0)
      digitalWrite(led.pin, 0);
    else
      //assign the timer to LED
      led.timerID = ledTimer.setInterval(delayMs, led.toggleFunc);

    newConfig=false;    
    inputString = "";
  }
}

//function gets called everytime data gets received through the serial interface
void serialEvent() {  
  while (Serial.available()) {
    //read bytes of of serial connection and add them to the configString
    char inChar = (char)Serial.read();
    inputString += inChar;

    //if line is complete set flag to reconfigure blinking frequencies
    if (inChar == '\n') {
      newConfig = true;
      //remove trailing whitespace/newline
      inputString.trim();
    }
  }
}
