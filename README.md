# blinkenlights

Small project to control the frequency with which 3 LEDs blink through a serial protocol using a Python script that runs on the host system


# Setup
## Schematic
![schematic](blinkenlights.png)  
## Arduino Firmware
Download the SimpleTimer library from [GitHub](https://github.com/jfturcot/SimpleTimer) as a zip-file and install it in the Arduino environment via Sketch -> Include library -> Add .ZIP library in the Arduino IDE.  
Open the Arduino Sketch located in firmware/arduino_firmware using the Arduino IDE and flash the Arduino with it. Note: the Sketch was only tested with an Arduino Uno.  
Connect 3 LEDS and appropriate resistors to the D5, D6 and D7 pins of the Arduino and GND (see schematic).
## Host Script
The host script needs the ``pyserial`` library, this can be installed with a simple ``pip install pyserial``.

# Usage
## Arduino Firmware
Connect to the UART connection of the Arduino with an appropriate serial monitor, for example the one in the Arduino IDE. Use a Baudrate of **9600 Baud**.
To change the the frequencies of the LEDs send lines of the format:
```
led_num:frequency
```
Here ``led_num`` is an integer between 0 and 2 indicating the LED of which to change the frequency and ``frequency`` is an integer indicating the frequency with which the LED should blink.  
For example the input ``0:50`` would result in the first LED blinking with a frequency of 50 Hertz.

## Host Script
The Host script expects two commandline arguments, the serial port to which it should connect and the data file from which it should read the commands. The script can then be run like ``./serial_driver.py serial_port data_file``. A valid call of the script would be ``./serial_driver.py /dev/ttyACM0 data.txt``. An example data file is provided.  
The format of the command file is
```
delay led_num blink_frequency
```
Where ``delay`` indicates a delay in ms before the execution of this line, ``led_num`` indicates the LED which the command applies to and ``blink_frequency`` the frequency with which the LED should blink.
