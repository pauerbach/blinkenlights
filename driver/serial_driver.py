#!/usr/bin/env python3

import serial
import time
import sys

def main():
    print("Starting serial driver")

    #TODO handle case where less than 2 arguments are provided

    #get serial port and filename from commandline arguments
    port = sys.argv[1]
    file_name = sys.argv[2]

    print("Setting up serial connection")
    #TODO handle error during opening of serial port
    ser = serial.Serial(port, 9600, timeout=1)
    time.sleep(2)

    print("Reading config file")
    #TODO handle error if file is not present
    myfile =  open(file_name, "r")
    data = myfile.readlines()

    for line in data:
        print(line.strip())

        #TODO handle case where there are more of less than three parameters per line

        parameters = line.split()
        time.sleep(int(parameters[0])/1000)
        ser.write(('%s:%s\n' % (parameters[1],parameters[2])).encode())
        ser.flush()

        #need a bit of delay for the Arduino to process the data
        time.sleep(0.03)

    ser.close()

if __name__ == "__main__":
    main()

